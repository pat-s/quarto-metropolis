# quarto-metropolis

Opinionated port of the Metropolis theme for Quarto presentations.
This project is an updated version of the [xaringan-metropolis theme for the {xaringan} R package](https://github.com/pat-s/xaringan-metropolis).

## Changes to the original Metropolis theme

This theme port applies some opinionated changes to the original upstream implementation from [matze/mtheme](https://github.com/matze/mtheme):

- Font: Use of "Roboto" instead of "Fira Sans"
- Code font: "JetBrains Mono"
- Gray column background
- Theme-compliant progress bars and hamburger menus


## Usage

**Caution**: this method is currently not working due to an upstream issue in `quarto`, see https://codeberg.org/pat-s/quarto-metropolis/issues/1.

If you already have a quarto presentation skeleton:

```sh
quarto install extension https://codeberg.org/pat-s/quarto-metropolis/archive/v1.0.1.zip
```

Or download/copy `_extensions/metropolis-theme/metropolis.scss` to your Quarto presentation and reference it in the YAML.

Unfortunately referencing an online resource in the YAML header is currently [not supported by quarto](https://github.com/quarto-dev/quarto-cli/discussions/1158).

For a quickstart with a new presentation skeleton, do

```sh
quarto use template https://codeberg.org/pat-s/quarto-metropolis/archive/v1.0.1.zip
```

## Usage remarks

- Columns **may not sum up to 100% width** as some space is needed for the custom gray background around the columns. Each column must be reduced by 3% width, i.e. with two columns, the sum may only be 94% width.
- Level 1 headers (`#`) are reserved for "section slides", i.e. centered slides which only contain the title. These slides are then automatically added to the Table of Contents (if enabled).

## Screenshots

![Screenshot of title slide](fig/ex3.png)
![Screenshot of section slide](fig/ex2.png)
![Screenshot of columns](fig/ex1.png)
![Screenshot of code](fig/ex4.png)
